package com.devmasterteam.photicker.views;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.devmasterteam.photicker.R;
import com.devmasterteam.photicker.utils.ImageUtil;
import com.devmasterteam.photicker.utils.LongEventType;
import com.devmasterteam.photicker.utils.PermissionUtil;
import com.devmasterteam.photicker.utils.SocialUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, View.OnLongClickListener {

    private static final int REQUEST_TAKE_PHOTO = 2;
    private final ViewHolder mViewHolder = new ViewHolder();
    private ImageView mImageSelected;
    private boolean mAutoIncrement;
    private Handler mRepeatUpdateHandle = new Handler();
    private LongEventType mLongEventType;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_take_photo:
                if (!PermissionUtil.hasCameraPermission(this)){
                    PermissionUtil.asksCameraPermission(this);
                }else {
                    dispatchTakePictureIntent();
                }
                break;
            case R.id.image_zoom_in:
                ImageUtil.HandleZoomIn(this.mImageSelected);
                break;
            case R.id.image_zoom_out:
                ImageUtil.HandleZoomOut(this.mImageSelected);
                break;
            case R.id.image_rotate_left:
                ImageUtil.HandleRotateLeft(this.mImageSelected);
                break;
            case R.id.image_rotate_right:
                ImageUtil.HandleRotateRight(this.mImageSelected);
                break;
            case R.id.image_finish:
                toggleControlPanel(false);
                break;
            case R.id.image_remove:
                this.mViewHolder.mRelativePhotoContent.removeView(this.mImageSelected);
                toggleControlPanel(false);
                break;
            case R.id.image_face:
                SocialUtil.shareImageOnFace(this, mViewHolder.mRelativePhotoContent, v);
                break;
            case R.id.image_insta:
                SocialUtil.shareImageOnInsta(this, mViewHolder.mRelativePhotoContent, v);
                break;
            case R.id.image_whats:
                SocialUtil.shareImageOnWhats(this, mViewHolder.mRelativePhotoContent, v);
                break;
            case R.id.image_twitter:
                SocialUtil.shareImageOnTwitter(this, mViewHolder.mRelativePhotoContent, v);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK){
            this.setPhotoAsBackground();
        }
    }

    private void setPhotoAsBackground() {
        int targetW = this.mViewHolder.mImagePhoto.getWidth();
        int targetH = this.mViewHolder.mImagePhoto.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.mViewHolder.mUriPhotoPath.getPath(), bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        Bitmap bitmap = BitmapFactory.decodeFile(this.mViewHolder.mUriPhotoPath.getPath(), bmOptions);

        Bitmap bitmapRotated = ImageUtil.rotateImageIfRequired(bitmap, this.mViewHolder.mUriPhotoPath);

        this.mViewHolder.mImagePhoto.setImageBitmap(bitmapRotated);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtil.CAMERA_PERMISSION){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                dispatchTakePictureIntent();
            }else {
                new AlertDialog.Builder(this)
                        .setMessage(getString(R.string.without_permission_camera_explanation))
                        .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null){
            File photo = null;
            try {
                photo = ImageUtil.createImageFile(this);
                this.mViewHolder.mUriPhotoPath = Uri.fromFile(photo);
            }catch (IOException e){
                Toast.makeText(this, "Não foi possível iniciar a câmera", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            if (photo != null){
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == R.id.image_zoom_in) this.mLongEventType = LongEventType.ZoomIn;
        if (v.getId() == R.id.image_zoom_out) this.mLongEventType = LongEventType.ZoomOut;
        if (v.getId() == R.id.image_rotate_left) this.mLongEventType = LongEventType.RotateLeft;
        if (v.getId() == R.id.image_rotate_right) this.mLongEventType = LongEventType.RotateRight;
        mAutoIncrement = true;
        new RptUpdater().run();

        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int id = v.getId();

        if (id == R.id.image_zoom_in ||
                id == R.id.image_zoom_out ||
                id == R.id.image_rotate_left ||
                id == R.id.image_rotate_right) {

            if (event.getAction() == MotionEvent.ACTION_UP && mAutoIncrement){
                mAutoIncrement = false;
                this.mLongEventType = null;
            }
        }
        return false;
    }

    private static class ViewHolder {
        LinearLayout mLinearSharePanel;
        LinearLayout mLinearControlPanel;

        ImageView mButtonZoomIn;
        ImageView mButtonZoomOut;
        ImageView mButtonRotateLeft;
        ImageView mButtonRotateRight;
        ImageView mButtonFinish;
        ImageView mButtonRemove;

        ImageView mImagePhoto;
        RelativeLayout mRelativePhotoContent;
        Uri mUriPhotoPath;

        ImageView mImageInstagram;
        ImageView mImageTwitter;
        ImageView mImageFb;
        ImageView mImageWhats;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        List<Integer> mListImages = ImageUtil.getImagesList();


        final LinearLayout content = (LinearLayout) this.findViewById(R.id.linear_horizontal_scroll_content);
        this.mViewHolder.mRelativePhotoContent = (RelativeLayout) this.findViewById(R.id.relative_photo_content_draw);

        for (Integer image_Id  : mListImages){
            ImageView image = new ImageView(this);
            image.setImageBitmap(ImageUtil.decodeSampledBitmapFromResource(getResources(), image_Id, 70, 70));
            image.setPadding(20, 10, 20, 10);

            BitmapFactory.Options dimensions = new BitmapFactory.Options();
            dimensions.inJustDecodeBounds = true;

            BitmapFactory.decodeResource(getResources(), image_Id, dimensions);

            final int width = dimensions.outWidth;
            final int height = dimensions.outHeight;

            image.setOnClickListener(onClickImageOption(this.mViewHolder.mRelativePhotoContent, image_Id, width, height));
            content.addView(image);

            this.setListeners();

        }

        this.mViewHolder.mLinearControlPanel = (LinearLayout) findViewById(R.id.linear_control_panel);
        this.mViewHolder.mLinearSharePanel= (LinearLayout) findViewById(R.id.linear_share_panel);

        this.mViewHolder.mButtonZoomIn = (ImageView) findViewById(R.id.image_zoom_in);
        this.mViewHolder.mButtonZoomOut = (ImageView) findViewById(R.id.image_zoom_out);
        this.mViewHolder.mButtonRotateLeft= (ImageView) findViewById(R.id.image_rotate_left);
        this.mViewHolder.mButtonRotateRight= (ImageView) findViewById(R.id.image_rotate_right);
        this.mViewHolder.mButtonFinish= (ImageView) findViewById(R.id.image_finish);
        this.mViewHolder.mButtonRemove= (ImageView) findViewById(R.id.image_remove);

        this.mViewHolder.mImagePhoto = (ImageView) findViewById(R.id.image_photo);

        this.mViewHolder.mImageInstagram = (ImageView) findViewById(R.id.image_insta);
        this.mViewHolder.mImageTwitter = (ImageView) findViewById(R.id.image_twitter);
        this.mViewHolder.mImageFb = (ImageView) findViewById(R.id.image_face);
        this.mViewHolder.mImageWhats = (ImageView) findViewById(R.id.image_whats);

    }

    private void setListeners() {
        this.findViewById(R.id.image_zoom_in).setOnClickListener(this);
        this.findViewById(R.id.image_zoom_out).setOnClickListener(this);
        this.findViewById(R.id.image_rotate_right).setOnClickListener(this);
        this.findViewById(R.id.image_rotate_left).setOnClickListener(this);
        this.findViewById(R.id.image_finish).setOnClickListener(this);
        this.findViewById(R.id.image_remove).setOnClickListener(this);

        this.findViewById(R.id.image_twitter).setOnClickListener(this);
        this.findViewById(R.id.image_insta).setOnClickListener(this);
        this.findViewById(R.id.image_face).setOnClickListener(this);
        this.findViewById(R.id.image_whats).setOnClickListener(this);

        this.findViewById(R.id.image_zoom_in).setOnLongClickListener(this);
        this.findViewById(R.id.image_zoom_out).setOnLongClickListener(this);
        this.findViewById(R.id.image_rotate_right).setOnLongClickListener(this);
        this.findViewById(R.id.image_rotate_left).setOnLongClickListener(this);

        this.findViewById(R.id.image_zoom_in).setOnTouchListener(this);
        this.findViewById(R.id.image_zoom_out).setOnTouchListener(this);
        this.findViewById(R.id.image_rotate_right).setOnTouchListener(this);
        this.findViewById(R.id.image_rotate_left).setOnTouchListener(this);

        this.findViewById(R.id.image_take_photo).setOnClickListener(this);
    }

    private View.OnClickListener onClickImageOption(final RelativeLayout relativeLayout, final Integer image_id, int width, int height) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ImageView imageView = new ImageView(MainActivity.this);
                imageView.setBackgroundResource(image_id);
                relativeLayout.addView(imageView);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();

                layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);

                mImageSelected = imageView;

                toggleControlPanel(true);

                imageView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent motionEvent) {

                        float x, y;

                        switch (motionEvent.getAction()){
                            case MotionEvent.ACTION_DOWN:
                                mImageSelected = imageView;
                                toggleControlPanel(true);
                                break;
                            case MotionEvent.ACTION_MOVE:
                                int coords[] = {0,0};
                                relativeLayout.getLocationOnScreen(coords);
                                x = (motionEvent.getRawX() - (imageView.getWidth()/2 ));
                                y = (motionEvent.getRawY() - ((coords[1] + 100) + (imageView.getHeight() /2)));
                                imageView.setX(x);
                                imageView.setY(y);
                                break;
                            case MotionEvent.ACTION_UP:
                                break;


                        }
                        return true;
                    }
                });

            }
        };
    }

    private void toggleControlPanel(boolean showControls) {
        if (showControls){
            this.mViewHolder.mLinearControlPanel.setVisibility(View.VISIBLE);
            this.mViewHolder.mLinearSharePanel.setVisibility(View.GONE);

        }else{
            this.mViewHolder.mLinearControlPanel.setVisibility(View.GONE);
            this.mViewHolder.mLinearSharePanel.setVisibility(View.VISIBLE);
        }
    }


    private class RptUpdater implements Runnable {
        @Override
        public void run() {
            if (mAutoIncrement)
                mRepeatUpdateHandle.postDelayed(new RptUpdater(), 50);

            if (mLongEventType != null){
                switch (mLongEventType){
                    case ZoomIn:
                        ImageUtil.HandleZoomIn(mImageSelected);
                        break;
                    case ZoomOut:
                        ImageUtil.HandleZoomOut(mImageSelected);
                        break;
                    case RotateLeft:
                        ImageUtil.HandleRotateLeft(mImageSelected);
                        break;
                    case RotateRight:
                        ImageUtil.HandleRotateRight(mImageSelected);
                        break;
                }
            }
        }
    }
}